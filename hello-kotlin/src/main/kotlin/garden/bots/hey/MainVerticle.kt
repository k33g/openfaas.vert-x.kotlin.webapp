package garden.bots.hey

import io.vertx.core.AbstractVerticle
import io.vertx.core.Promise
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.StaticHandler


class MainVerticle : AbstractVerticle() {

  override fun start(startPromise: Promise<Void>) {

    val server = vertx.createHttpServer()

    val router = Router.router(vertx)
    router.route().handler(BodyHandler.create())

    router.route("/*").handler(StaticHandler.create())


    router.get("/hi").handler { context ->
      context.response()
        .putHeader("content-type", "application/json;charset=UTF-8")
        .end(JsonObject().put("message", "👋 Hi 😃").encodePrettily())
    }

    router.get("/hello").handler { context ->
      context.response()
        .putHeader("content-type", "application/json;charset=UTF-8")
        .end(JsonObject().put("message", "🖖 hello world 🌍").encodePrettily())

    }

    server.requestHandler(router).listen(8080) { httpResult ->
      if (httpResult.succeeded()) {
        startPromise.complete()
        println("🌍 Listening on 8080")
      } else {
        startPromise.fail("😡 Houston, we have a problem")
        println("😡 Houston, we have a problem: " + httpResult.cause())
      }
    }

  }
}
