# openfaas.kotlin.webapp

## What?

- This project is a web application written with [Kotlin](https://kotlinlang.org/) and [Vert-x](https://vertx.io/) 
- The main goal of this project is to be deployed on [OpenFaaS](https://www.openfaas.com/) but not as a function, but a web application.

It means that I'm using [OpenFaaS](https://www.openfaas.com/) like a PaaS. You can read more about this here: [https://www.openfaas.com/blog/stateless-microservices/](https://www.openfaas.com/blog/stateless-microservices/)

## Requirements

- Install an [OpenFaaS](https://www.openfaas.com/) platform
- Install the OpenFaaS cli

## How to deploy the web application?

### Build jar file

First, you need to build the `jar` file of the Vert-x application:

```shell 
cd hello-kotlin
./mvnw clean package
```

It will produce a fat `jar` file: `hello-kotlin-1.0.0-SNAPSHOT-fat.jar` in the `target` directory.

### Deploy to OpenFaaS

The `faas-cli` need the `hello-kotlin.yml` file and some environment variables, and then you can build the image, push it to the docker hub and then deploy to your OpenFaaS platform:

```shell 
export DOCKER_HANDLE="your_docker_hub_handle"
export DOCKER_PASSWORD="your_docker_hub_password"
export OPENFASS_TOKEN="your_openfaas_token"
export OPENFAAS_URL="openfaas_url" # eg: http://openfaas.test:8080

echo -n ${DOCKER_PASSWORD} | docker login --username ${DOCKER_HANDLE} --password-stdin
echo -n ${OPENFASS_TOKEN} | faas-cli login --username=admin --password-stdin

faas-cli build -f hello-kotlin.yml
faas-cli push -f hello-kotlin.yml
faas-cli deploy -f hello-kotlin.yml

# get the logs of the webapplication
faas-cli logs hello-kotlin
```

You can access to the application: [http://openfaas_url/function/hello-kotlin](http://openfaas_url/function/hello-kotlin) and to the json services:
- [http://openfaas_url/function/hello-kotlin/hi](http://openfaas_url/function/hello-kotlin/hi)
- [http://openfaas_url/function/hello-kotlin/hello](http://openfaas_url/function/hello-kotlin/hello)
